from abc import ABC
from dataclasses import dataclass, field
from enum import Enum
from typing import List, Optional

from svg_renderer.common import Color, Point, Rectangle


class SVGElement(ABC):
    pass


@dataclass
class SVG:
    width: int
    height: int
    elements: List[SVGElement] = field(default_factory=list)

    view_box: Optional[Rectangle] = None


@dataclass
class Rect(SVGElement):
    x: int
    y: int
    width: int
    height: int

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None
    fill: Optional[Color] = None


@dataclass
class Circle(SVGElement):
    cx: int
    cy: int
    r: int

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None
    fill: Optional[Color] = None


@dataclass
class Ellipse(SVGElement):
    cx: int
    cy: int
    rx: int
    ry: int

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None
    fill: Optional[Color] = None


@dataclass
class Line(SVGElement):
    start: Point
    end: Point

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None


@dataclass
class Polyline(SVGElement):
    points: List[Point]

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None


@dataclass
class Polygon(SVGElement):
    points: List[Point]

    stroke: Optional[Color] = None
    stroke_width: Optional[int] = None
    fill: Optional[Color] = None


class PathCommand(ABC):
    pass


@dataclass
class Path(SVGElement):
    commands: List[PathCommand]

    stroke_color: Optional[Color] = None


@dataclass
class MoveCommand(PathCommand):
    x: int
    y: int

    is_relative: bool = False


@dataclass
class LineCommand(PathCommand):
    x: int
    y: int

    is_relative: bool = False


@dataclass
class VerticalLineCommand(PathCommand):
    y: int

    is_relative: bool = False


@dataclass
class HorizontalLineCommand(PathCommand):
    x: int

    is_relative: bool = False


@dataclass
class CloseCommand(PathCommand):
    pass


@dataclass
class CubicBezierCommand(PathCommand):
    pivot1: Point
    pivot2: Point
    end: Point

    is_relative: bool = False


@dataclass
class ShortCubicBezierCommand(PathCommand):
    pivot2: Point
    end: Point

    is_relative: bool = False


@dataclass
class SquareBezierCommand(PathCommand):
    pivot: Point
    end: Point

    is_relative: bool = False


@dataclass
class ShortSquareBezierCommand(PathCommand):
    end: Point

    is_relative: bool = False


class FontStyle(str, Enum):
    NORMAL = "normal"
    ITALIC = "italic"
    OBLIQUE = "oblique"


@dataclass
class Text(SVGElement):
    x: int
    y: int
    content: str

    fill: Optional[Color] = None
    font_family: Optional[str] = None
    font_size: Optional[int] = None
    font_style: Optional[FontStyle] = None
