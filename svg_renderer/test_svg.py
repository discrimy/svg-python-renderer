from svg_renderer.common import Point, BLACK, GREY, RED, Rectangle, Color, WHITE

from svg_renderer.svg import (
    SVG,
    Rect,
    Circle,
    Ellipse,
    Line,
    Polyline,
    Polygon,
    Path,
    MoveCommand,
    VerticalLineCommand,
    HorizontalLineCommand,
    CloseCommand,
    CubicBezierCommand,
    SquareBezierCommand,
    ShortSquareBezierCommand,
    Text,
    FontStyle,
)

svg = SVG(
    500,
    400,
    elements=[
        Rect(20, 30, 100, 200, stroke=RED, fill=GREY),
        Rect(100, 200, 100, 50),
        Circle(100, 100, 30, fill=RED, stroke=BLACK),
        Ellipse(200, 200, 50, 80, stroke=GREY),
        Line(Point(10, 10), Point(200, 100), stroke=RED, stroke_width=30),
        Polyline([Point(110, 110), Point(120, 130), Point(150, 290)], stroke_width=10),
        Polygon(
            [Point(10, 110), Point(120, 130), Point(150, 290)],
            fill=Color(255, 255, 0, 128),
            stroke=Color(0, 255, 255),
            stroke_width=20,
        ),
        Path(
            [
                MoveCommand(10, 10),
                VerticalLineCommand(50, is_relative=True),
                HorizontalLineCommand(100, is_relative=True),
                VerticalLineCommand(-50, is_relative=True),
                CloseCommand(),
                CubicBezierCommand(
                    Point(100, 0), Point(0, 100), Point(0, 0), is_relative=True
                ),
                SquareBezierCommand(Point(100, 100), Point(50, 150), is_relative=True),
                ShortSquareBezierCommand(Point(250, 50)),
            ],
            stroke_color=Color(94, 30, 30),
        ),
        Text(50, 50, "Test text!", fill=WHITE, font_size=20),
        Text(
            150,
            20,
            "Hello world!",
            font_size=32,
            font_family="Times New Roman",
            font_style=FontStyle.ITALIC,
        ),
    ],
    view_box=Rectangle(10, 0, 300, 300),
)
