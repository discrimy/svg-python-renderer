from __future__ import annotations

import itertools
import math
import re
from typing import NamedTuple, TypeVar, Iterable, Tuple, List, Generator


class Color(NamedTuple):
    r: int
    g: int
    b: int
    a: int = 255

    @classmethod
    def from_name(cls, name: str) -> Color:
        try:
            return {
                "red": RED,
                "black": BLACK,
                "grey": GREY,
                "white": WHITE,
                "transparent": TRANSPARENT,
                "green": GREEN,
                "orange": ORANGE,
                "blue": BLUE,
            }[name]
        except KeyError:
            raise ValueError(f"Unknown color name: {name}")

    @classmethod
    def from_rgb(cls, raw_rgb: str) -> Color:
        match = re.match(r"^rgb\((\d+),(\d+),(\d+)\)$", raw_rgb)
        if match is None:
            raise ValueError(f"Format is not rgb: {raw_rgb}")
        r, g, b = match.groups()
        return cls(int(r), int(g), int(b))

    @classmethod
    def from_rgba(cls, raw_rgba: str) -> Color:
        match = re.match(r"^rgba\((\d+),(\d+),(\d+),(\d+\.\d+)?\)$", raw_rgba)
        if match is None:
            raise ValueError(f"Format is not rgba: {raw_rgba}")
        r, g, b, a = match.groups()
        return cls(int(r), int(g), int(b), int(float(a) * 255))


TRANSPARENT = Color(0, 0, 0, 0)
WHITE = Color(255, 255, 255)
BLACK = Color(0, 0, 0)
GREY = Color(128, 128, 128)
RED = Color(255, 0, 0)
GREEN = Color(0, 255, 0)
ORANGE = Color(255, 165, 0)
BLUE = Color(0, 0, 255)


class Point(NamedTuple):
    x: float
    y: float

    def length(self) -> float:
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __add__(self, other: Point) -> Point:
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other: Point) -> Point:
        return self + (-other)

    def __neg__(self) -> Point:
        return Point(-self.x, -self.y)

    def __mul__(self, other: float) -> Point:
        return Point(self.x * other, self.y * other)

    def __truediv__(self, other: float) -> Point:
        return self * (1 / other)


class Rectangle(NamedTuple):
    x: int
    y: int
    width: int
    height: int


T = TypeVar("T")


def pairwise(iterable: Iterable[T]) -> Iterable[Tuple[T, T]]:
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def float_range(start: float, end: float, delta: float) -> Generator[float, None, None]:
    while start < end:
        yield start
        start += delta


def find_bezier_point(t: float, point1: Point, point2: Point, *other: Point) -> Point:
    """
    Recursive formula of Bezier line point calculation
    :param t: from 0 (begging) to 1 (end)
    :param point1: begging of line
    :param point2: second pivot point, can be the last
    :param other: other pivots, can be empty
    """
    if not other:
        return point1 * (1 - t) + point2 * t
    else:
        points = [
            p1 * (1 - t) + p2 * t for p1, p2 in pairwise([point1, point2, *other])
        ]
        return find_bezier_point(t, *points)


def get_mirrored_point(mirror: Point, point: Point) -> Point:
    return (mirror * 2) - point


def get_bezier_pivots(
    current_pos: Point, other_pivots: List[Point], is_relative: bool
) -> List[Point]:
    if is_relative:
        return [current_pos, *[pivot + current_pos for pivot in other_pivots]]
    else:
        return [current_pos, *other_pivots]
