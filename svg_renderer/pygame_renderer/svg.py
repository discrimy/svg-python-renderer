import functools
from functools import singledispatchmethod
from typing import Tuple, List

import pygame
import pygame.freetype

from svg_renderer.common import (
    Rectangle,
    Point,
    pairwise,
    WHITE,
)
from svg_renderer.pygame_renderer import defaults
from svg_renderer.pygame_renderer.defaults import up_to_pixels
from svg_renderer.pygame_renderer.path import PathRenderer
from svg_renderer.svg import (
    Rect,
    Circle,
    Ellipse,
    Line,
    Polyline,
    Polygon,
    Path,
    SVG,
    SVGElement,
    Text,
    FontStyle,
)


@functools.cache
def _get_or_create_font(font_family: str, font_size: int) -> pygame.freetype.Font:
    return pygame.freetype.SysFont(font_family, font_size)


class SVGRenderer:
    def __init__(
        self, screen: pygame.Surface, svg: SVG, view_box: Rectangle, ppu: float
    ):
        self.svg = svg
        self.screen = screen
        self.view_box = view_box
        self.ppu = ppu

    def _to_pixels(self, user_points: float) -> int:
        return up_to_pixels(user_points, self.ppu)

    def process(self):
        for element in self.svg.elements:
            self.process_element(element)

    @singledispatchmethod
    def process_element(self, element: SVGElement) -> None:
        raise NotImplementedError

    @process_element.register
    def _(self, rect: Rect) -> None:
        fill_color = rect.fill or defaults.FILL_COLOR
        stroke_color = rect.stroke or defaults.STROKE_COLOR
        stroke_width = rect.stroke_width or defaults.STROKE_WIDTH

        rect_sizes = (
            self._to_pixels(rect.x - self.view_box.x),
            self._to_pixels(rect.y - self.view_box.y),
            self._to_pixels(rect.width),
            self._to_pixels(rect.height),
        )
        pygame.draw.rect(
            self.screen,
            fill_color,
            rect_sizes,
        )
        pygame.draw.rect(
            self.screen,
            stroke_color,
            rect_sizes,
            width=self._to_pixels(stroke_width),
        )

    @process_element.register
    def _(self, circle: Circle) -> None:
        fill_color = circle.fill or defaults.FILL_COLOR
        stroke_color = circle.stroke or defaults.STROKE_COLOR
        stroke_width = circle.stroke_width or defaults.STROKE_WIDTH

        circle_center = (
            self._to_pixels(circle.cx - self.view_box.x),
            self._to_pixels(circle.cy - self.view_box.y),
        )
        circle_radius = self._to_pixels(circle.r)
        pygame.draw.circle(
            self.screen,
            fill_color,
            circle_center,
            circle_radius,
        )
        pygame.draw.circle(
            self.screen,
            stroke_color,
            circle_center,
            circle_radius,
            width=self._to_pixels(stroke_width),
        )

    @process_element.register
    def _(self, ellipse: Ellipse) -> None:
        fill_color = ellipse.fill or defaults.FILL_COLOR
        stroke_color = ellipse.stroke or defaults.STROKE_COLOR
        stroke_width = ellipse.stroke_width or defaults.STROKE_WIDTH
        rect = (
            self._to_pixels(ellipse.cx - ellipse.rx - self.view_box.x),
            self._to_pixels(ellipse.cy - ellipse.ry - self.view_box.y),
            self._to_pixels(ellipse.rx * 2),
            self._to_pixels(ellipse.ry * 2),
        )

        pygame.draw.ellipse(self.screen, fill_color, rect)
        pygame.draw.ellipse(
            self.screen, stroke_color, rect, width=self._to_pixels(stroke_width)
        )

    @process_element.register
    def _(self, line: Line) -> None:
        stroke_color = line.stroke or defaults.FILL_COLOR
        stroke_width = line.stroke_width or defaults.STROKE_WIDTH

        # Emulate line width with polygon
        points = [
            Point(self._to_pixels(point.x), self._to_pixels(point.y))
            for point in line_to_polygon_points(
                (line.start, line.end), stroke_width, self.view_box
            )
        ]

        pygame.draw.polygon(self.screen, stroke_color, points)

    @process_element.register
    def _(self, polyline: Polyline) -> None:
        stroke_color = polyline.stroke or defaults.FILL_COLOR
        stroke_width = polyline.stroke_width or defaults.STROKE_WIDTH

        # Emulate lines with polygons
        polygons_points = [
            [
                Point(self._to_pixels(point.x), self._to_pixels(point.y))
                for point in line_to_polygon_points(
                    (start, end), stroke_width, self.view_box
                )
            ]
            for start, end in pairwise(polyline.points)
        ]

        for points in polygons_points:
            pygame.draw.polygon(self.screen, stroke_color, points)

    @process_element.register
    def _(self, polygon: Polygon) -> None:
        fill_color = polygon.fill or defaults.FILL_COLOR
        stroke_color = polygon.stroke or defaults.STROKE_COLOR
        stroke_width = polygon.stroke_width or defaults.STROKE_WIDTH
        points = [
            Point(
                self._to_pixels(point.x - self.view_box.x),
                self._to_pixels(point.y - self.view_box.y),
            )
            for point in polygon.points
        ]

        pygame.draw.polygon(self.screen, fill_color, points)
        pygame.draw.polygon(
            self.screen, stroke_color, points, width=self._to_pixels(stroke_width)
        )

    @process_element.register
    def _(self, path: Path) -> None:
        renderer = PathRenderer(self.screen, self.view_box, path, ppu=self.ppu)
        renderer.process()

    @process_element.register
    def _(self, text: Text) -> None:
        fill_color = text.fill or defaults.FILL_COLOR
        font_family = text.font_family or defaults.FONT_FAMILY
        font_size = text.font_size or defaults.FONT_SIZE
        font_style = text.font_style or FontStyle.NORMAL

        font = _get_or_create_font(font_family, self._to_pixels(font_size))
        pygame_font_style = {
            FontStyle.NORMAL: pygame.freetype.STYLE_NORMAL,
            # Italic and oblique are the same at most time
            FontStyle.ITALIC: pygame.freetype.STYLE_OBLIQUE,
            FontStyle.OBLIQUE: pygame.freetype.STYLE_OBLIQUE,
        }[font_style]
        text_surface, rect = font.render(
            text.content, fill_color, style=pygame_font_style
        )
        self.screen.blit(
            text_surface,
            Point(
                self._to_pixels(text.x - self.view_box.x),
                self._to_pixels(text.y - self.view_box.y),
            ),
        )


def draw_via_pygame(svg: SVG, ppu: float = 1) -> None:
    view_box = svg.view_box or Rectangle(0, 0, svg.width, svg.height)
    size = (round(view_box.width * ppu), round(view_box.height * ppu))

    pygame.init()
    window = pygame.display.set_mode(size)
    screen = pygame.Surface(size, pygame.SRCALPHA)
    renderer = SVGRenderer(screen, svg, view_box, ppu)

    done = False
    clock = pygame.time.Clock()
    while not done:
        clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True

        window.fill(WHITE)
        renderer.process()
        window.blit(screen, (0, 0))
        pygame.display.flip()


def line_to_polygon_points(
    line: Tuple[Point, Point], stroke_width: int, view_box: Rectangle
) -> List[Point]:
    start, end = line
    line_point = end - start
    e = line_point / line_point.length()
    t = Point(e.y, -e.x)
    points = [
        start + (t * stroke_width / 2),
        start - (t * stroke_width / 2),
        end - (t * stroke_width / 2),
        end + (t * stroke_width / 2),
    ]
    points = [Point(point.x - view_box.x, point.y - view_box.y) for point in points]
    return points
