from svg_renderer.common import BLACK, TRANSPARENT

FILL_COLOR = BLACK
STROKE_COLOR = TRANSPARENT
STROKE_WIDTH = 1
FONT_FAMILY = "Arial"
FONT_SIZE = 16


def up_to_pixels(up: float, ppu: float) -> int:
    return round(up * ppu)