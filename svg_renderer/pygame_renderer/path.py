from functools import singledispatchmethod
from typing import List, Optional

import pygame

from svg_renderer.common import (
    Point,
    Color,
    find_bezier_point,
    float_range,
    Rectangle,
    BLACK,
    get_bezier_pivots,
    get_mirrored_point,
)
from svg_renderer.pygame_renderer.defaults import up_to_pixels
from svg_renderer.svg import (
    Path,
    CubicBezierCommand,
    ShortCubicBezierCommand,
    SquareBezierCommand,
    ShortSquareBezierCommand,
    PathCommand,
    MoveCommand,
    LineCommand,
    VerticalLineCommand,
    HorizontalLineCommand,
    CloseCommand,
)


def draw_path_line(
    screen: pygame.Surface,
    start: Point,
    end: Point,
    stroke_color: Color,
    view_box_shift: Point,
) -> None:
    pygame.draw.aaline(
        screen, stroke_color, start - view_box_shift, end - view_box_shift
    )


def draw_path_bezier_line(
    screen: pygame.Surface,
    pivots: List[Point],
    stroke_color: Color,
    view_box_shift: Point,
) -> None:
    line_points = [
        find_bezier_point(t, *pivots) - view_box_shift for t in float_range(0, 1, 0.01)
    ]
    pygame.draw.aalines(screen, stroke_color, closed=False, points=line_points)


class PathRenderer:
    def __init__(self, screen: pygame.Surface, view_box: Rectangle, path: Path, ppu: float):
        self.screen = screen
        self.view_box_shift = Point(view_box.x, view_box.y)
        self.ppu = ppu

        self.initial_pos: Optional[Point] = None
        self.current_pos = Point(0, 0)
        self.path = path
        self.stroke_color = path.stroke_color or BLACK

        self.last_cubic_pivot: Optional[Point] = None
        self.last_square_pivot: Optional[Point] = None

    def _to_pixels(self, user_points: float) -> int:
        return up_to_pixels(user_points, self.ppu)

    def _point_to_pixels(self, point: Point) -> Point:
        return Point(self._to_pixels(point.x), self._to_pixels(point.y))

    def process(self) -> None:
        for command in self.path.commands:
            self.process_command(command)

            if not isinstance(command, (CubicBezierCommand, ShortCubicBezierCommand)):
                self.last_cubic_pivot = None
            if not isinstance(command, (SquareBezierCommand, ShortSquareBezierCommand)):
                self.last_square_pivot = None

    @singledispatchmethod
    def process_command(self, command: PathCommand) -> None:
        raise NotImplementedError

    @process_command.register
    def _(self, command: MoveCommand) -> None:
        if command.is_relative:
            self.current_pos += Point(command.x, command.y)
        else:
            self.current_pos = Point(command.x, command.y)
        if self.initial_pos is None:
            self.initial_pos = self.current_pos

    @process_command.register
    def _(self, command: LineCommand) -> None:
        if command.is_relative:
            draw_to = Point(
                self.current_pos.x + command.x, self.current_pos.y + command.y
            )
        else:
            draw_to = Point(command.x, command.y)
        draw_path_line(
            self.screen,
            self._point_to_pixels(self.current_pos),
            self._point_to_pixels(draw_to),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )
        self.current_pos = draw_to

    @process_command.register
    def _(self, command: VerticalLineCommand) -> None:
        if command.is_relative:
            draw_to = Point(self.current_pos.x, self.current_pos.y + command.y)
        else:
            draw_to = Point(self.current_pos.x, command.y)
        draw_path_line(
            self.screen,
            self._point_to_pixels(self.current_pos),
            self._point_to_pixels(draw_to),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )
        self.current_pos = draw_to

    @process_command.register
    def _(self, command: HorizontalLineCommand) -> None:
        if command.is_relative:
            draw_to = Point(self.current_pos.x + command.x, self.current_pos.y)
        else:
            draw_to = Point(command.x, self.current_pos.y)
        draw_path_line(
            self.screen,
            self._point_to_pixels(self.current_pos),
            self._point_to_pixels(draw_to),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )
        self.current_pos = draw_to

    @process_command.register
    def _(self, _: CloseCommand) -> None:
        draw_to = self.initial_pos or Point(0, 0)
        draw_path_line(
            self.screen,
            self._point_to_pixels(self.current_pos),
            self._point_to_pixels(draw_to),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )
        self.current_pos = draw_to

    @process_command.register
    def _(self, command: CubicBezierCommand) -> None:
        pivots = get_bezier_pivots(
            self.current_pos,
            [command.pivot1, command.pivot2, command.end],
            command.is_relative,
        )
        draw_path_bezier_line(
            self.screen,
            list(map(self._point_to_pixels, pivots)),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )

        self.current_pos = command.end
        self.last_cubic_pivot = pivots[-2]

    @process_command.register
    def _(self, command: ShortCubicBezierCommand) -> None:
        if self.last_cubic_pivot is not None:
            pivot1 = get_mirrored_point(self.current_pos, self.last_cubic_pivot)
        else:
            pivot1 = command.pivot2

        pivots = get_bezier_pivots(
            self.current_pos, [pivot1, command.pivot2, command.end], command.is_relative
        )
        draw_path_bezier_line(
            self.screen,
            list(map(self._point_to_pixels, pivots)),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )

        self.current_pos = command.end
        self.last_cubic_pivot = pivots[-2]

    @process_command.register
    def _(self, command: SquareBezierCommand) -> None:
        pivots = get_bezier_pivots(
            self.current_pos, [command.pivot, command.end], command.is_relative
        )
        draw_path_bezier_line(
            self.screen,
            list(map(self._point_to_pixels, pivots)),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )

        self.current_pos = command.end
        self.last_square_pivot = pivots[-2]

    @process_command.register
    def _(self, command: ShortSquareBezierCommand) -> None:
        if self.last_square_pivot is not None:
            pivot = get_mirrored_point(self.current_pos, self.last_square_pivot)
        else:
            pivot = command.end

        pivots = get_bezier_pivots(
            self.current_pos, [pivot, command.end], command.is_relative
        )
        draw_path_bezier_line(
            self.screen,
            list(map(self._point_to_pixels, pivots)),
            self.stroke_color,
            self._point_to_pixels(self.view_box_shift),
        )

        self.current_pos = command.end
        self.last_square_pivot = pivots[-2]
