import pathlib

from svg_renderer.parser.svg import parse_svg_from_file
from svg_renderer.pygame_renderer.svg import draw_via_pygame


def main() -> None:
    svg = parse_svg_from_file(pathlib.Path("test_svg.svg"))
    draw_via_pygame(svg, ppu=.5)


if __name__ == "__main__":
    main()
