from typing import Optional

from svg_renderer.common import Color


def int_or_none(value: Optional[str]) -> Optional[int]:
    if value is None:
        return None
    else:
        return int(value)


def parse_color(raw_color: Optional[str]) -> Optional[Color]:
    if raw_color is None:
        return None

    try:
        return Color.from_name(raw_color)
    except ValueError:
        pass
    try:
        return Color.from_rgb(raw_color)
    except ValueError:
        pass
    try:
        return Color.from_rgba(raw_color)
    except ValueError:
        pass
    raise ValueError(f"Unknown format: {raw_color}")
