import pathlib
from typing import List
from xml.etree import ElementTree

from svg_renderer.common import Point
from svg_renderer.parser.path import parse_path_from_xml
from svg_renderer.parser.utils import int_or_none, parse_color
from svg_renderer.svg import (
    SVG,
    Rect,
    Circle,
    Text,
    Ellipse,
    Line,
    Polyline,
    Polygon,
    Path,
)


def parse_svg_from_xml(xml_tag: ElementTree) -> SVG:
    width = int(xml_tag.attrib["width"])
    height = int(xml_tag.attrib["height"])

    elements = [
        {
            "rect": parse_svg_rect,
            "circle": parse_svg_circle,
            "text": parse_svg_text,
            "ellipse": parse_svg_ellipse,
            "line": parse_svg_line,
            "polyline": parse_svg_polyline,
            "polygon": parse_svg_polygon,
            "path": parse_svg_path,
        }[tag.tag](tag)
        for tag in xml_tag
    ]

    return SVG(
        width=width,
        height=height,
        elements=elements,
    )


def parse_svg_rect(tag: ElementTree) -> Rect:
    return Rect(
        x=int(tag.attrib.get("x", 0)),
        y=int(tag.attrib.get("y", 0)),
        width=int(tag.attrib["width"]),
        height=int(tag.attrib["height"]),
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
        fill=parse_color(tag.attrib.get("fill")),
    )


def parse_svg_circle(tag: ElementTree) -> Circle:
    return Circle(
        cx=int(tag.attrib.get("cx", 0)),
        cy=int(tag.attrib.get("cy", 0)),
        r=int(tag.attrib["r"]),
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
        fill=parse_color(tag.attrib.get("fill")),
    )


def parse_svg_text(tag: ElementTree) -> Text:
    return Text(
        x=int(tag.attrib.get("x", 0)),
        y=int(tag.attrib.get("y", 0)),
        content=tag.text,
        fill=parse_color(tag.attrib.get("fill")),
        font_family=tag.attrib.get("font-family"),
        font_size=int_or_none(tag.attrib.get("font-size")),
    )


def parse_svg_ellipse(tag: ElementTree) -> Ellipse:
    return Ellipse(
        cx=int(tag.attrib.get("cx", 0)),
        cy=int(tag.attrib.get("cy", 0)),
        rx=int(tag.attrib["rx"]),
        ry=int(tag.attrib["ry"]),
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
        fill=parse_color(tag.attrib.get("fill")),
    )


def parse_svg_line(tag: ElementTree) -> Line:
    return Line(
        start=Point(int(tag.attrib["x1"]), int(tag.attrib["y1"])),
        end=Point(int(tag.attrib["x2"]), int(tag.attrib["y2"])),
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
    )


def parse_svg_polyline(tag: ElementTree) -> Polyline:
    coords = [int(raw) for raw in tag.attrib["points"].split(" ")]
    if len(coords) % 2 != 0:
        raise ValueError(
            f"List of points must consists of even number of integers, not {coords}"
        )
    points: List[Point] = [Point(*t) for t in zip(coords[0::2], coords[1::2])]
    return Polyline(
        points=points,
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
    )


def parse_svg_polygon(tag: ElementTree) -> Polygon:
    coords = [int(raw) for raw in tag.attrib["points"].split(" ")]
    if len(coords) % 2 != 0:
        raise ValueError(
            f"List of points must consists of even number of integers, not {coords}"
        )
    points: List[Point] = [Point(*t) for t in zip(coords[0::2], coords[1::2])]
    return Polygon(
        points=points,
        stroke=parse_color(tag.attrib.get("stroke")),
        stroke_width=int_or_none(tag.attrib.get("stroke-width")),
        fill=parse_color(tag.attrib.get("fill")),
    )


def parse_svg_path(tag: ElementTree) -> Path:
    return parse_path_from_xml(tag)


def parse_svg_from_file(file_path: pathlib.Path) -> SVG:
    return parse_svg_from_xml(ElementTree.parse(file_path).getroot())
