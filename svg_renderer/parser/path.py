import re
from typing import List, Tuple
from xml.etree import ElementTree

from svg_renderer.common import Point
from svg_renderer.parser.utils import parse_color
from svg_renderer.svg import (
    Path,
    ShortSquareBezierCommand,
    SquareBezierCommand,
    ShortCubicBezierCommand,
    CubicBezierCommand,
    CloseCommand,
    VerticalLineCommand,
    HorizontalLineCommand,
    LineCommand,
    MoveCommand,
    PathCommand,
)


def parse_path_from_xml(xml_tag: ElementTree) -> Path:
    params: List[str] = re.findall(r"[\d-]+|\w", xml_tag.attrib["d"])
    commands_raw: List[Tuple[str, List[int]]] = []
    for param in params:
        if param.isalpha():
            commands_raw.append((param, []))
        else:
            commands_raw[-1][1].append(int(param))

    commands: List[PathCommand] = []
    for name, args in commands_raw:
        is_relative = name.islower()
        commands.append(
            {
                "M": parse_move_command,
                "L": parse_line_command,
                "H": parse_horizontal_line_command,
                "V": parse_vertical_line_command,
                "Z": parse_close_command,
                "C": parse_cubic_bezier_command,
                "S": parse_short_cubic_bezier_command,
                "Q": parse_square_bezier_command,
                "T": parse_short_square_bezier_command,
            }[name.upper()](is_relative, *args)
        )

    return Path(
        commands=commands, stroke_color=parse_color(xml_tag.attrib.get("stroke"))
    )


def parse_move_command(is_relative: bool, x: int, y: int) -> MoveCommand:
    return MoveCommand(x, y, is_relative=is_relative)


def parse_line_command(is_relative: bool, x: int, y: int) -> LineCommand:
    return LineCommand(x, y, is_relative=is_relative)


def parse_horizontal_line_command(is_relative: bool, x: int) -> HorizontalLineCommand:
    return HorizontalLineCommand(x, is_relative=is_relative)


def parse_vertical_line_command(is_relative: bool, y: int) -> VerticalLineCommand:
    return VerticalLineCommand(y, is_relative=is_relative)


def parse_close_command(is_relative: bool) -> CloseCommand:
    return CloseCommand()


def parse_cubic_bezier_command(
    is_relative: bool, x1: int, y1: int, x2: int, y2: int, x: int, y: int
) -> CubicBezierCommand:
    return CubicBezierCommand(
        pivot1=Point(x1, y1),
        pivot2=Point(x2, y2),
        end=Point(x, y),
        is_relative=is_relative,
    )


def parse_short_cubic_bezier_command(
    is_relative: bool, x2: int, y2: int, x: int, y: int
) -> ShortCubicBezierCommand:
    return ShortCubicBezierCommand(
        pivot2=Point(x2, y2),
        end=Point(x, y),
        is_relative=is_relative,
    )


def parse_square_bezier_command(
    is_relative: bool, x1: int, y1: int, x: int, y: int
) -> SquareBezierCommand:
    return SquareBezierCommand(
        pivot=Point(x1, y1),
        end=Point(x, y),
        is_relative=is_relative,
    )


def parse_short_square_bezier_command(
    is_relative: bool, x: int, y: int
) -> ShortSquareBezierCommand:
    return ShortSquareBezierCommand(
        end=Point(x, y),
        is_relative=is_relative,
    )
